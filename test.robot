***Settings***
Library             SeleniumLibrary

***Test Cases***
TC101 Testcase
    ${options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    # Call Method    ${options}    add_argument    --user-agent\=Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36 System/ComputerId
    Call Method    ${options}    add_argument    --user-agent\=Mozilla/5.0 (iPad; CPU OS 11_0 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Version/11.0 Mobile/15A5341f Safari/604.1
    Create WebDriver    Chrome    chrome_options=${options}    
    Go To    http://www.useragentstring.com

TC102 Testcase
    ${options}= Evaluate sys.modules['selenium.webdriver'].ChromeOptions() sys, selenium.webdriver
    ${userAgent}= set variable --user-agent=Mozilla/5.0 (iPad; CPU OS 11_0 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Version/11.0 Mobile/15A5341f Safari/604.1
    Call Method ${options} add_argument ${userAgent}
    Create WebDriver Chrome chrome_options=${options}
    Go To    http://www.useragentstring.com